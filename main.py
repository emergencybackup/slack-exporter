import os
import json
import time
from slackclient import SlackClient


def page_history(sc, channel, row_writer_fn):
    """
    Get throught the entire history of message for a channel
    """
    next_cursor = None
    while True:
        kwargs = {'channel': channel['id'], 'limit': 100}
        if next_cursor:
            kwargs['cursor'] = next_cursor
        history = sc.api_call('conversations.history', **kwargs)
        
        # Calls the entry writer for each message in the current page
        list(map(row_writer_fn, history['messages']))

        next_cursor = None if not history['has_more'] else history['response_metadata']['next_cursor'] 
        if not next_cursor:
            break

def new_store(obj):
    """
    Simply store messages in the memory, as free Slack have max 10k entries.
    retults will be inserted in the obj parameter
    """
    obj['x'] = []
    def __wrap__(msg):
        obj['x'] += [msg]
    return __wrap__



def export_category(sc, category, dest):
    conversations = sc.api_call('conversations.list', types=category)
    with open(os.path.join(dest, '_conversations.json'), 'w') as f:
        json.dump(conversations, f)
    
    for channel in conversations['channels']:
        chid = channel.get('name') if channel.get('name') is not None else channel.get('user')
        channel_file = os.path.join(dest, '{}.json'.format(chid))
        store = {}
        print('[{0: <12}] {1}'.format(chid, channel_file))
        ch_infos = sc.api_call('conversations.info', channel=channel['id'])
        page_history(sc, channel, new_store(store))
        with open(channel_file, 'w') as f:
            json.dump({
                'infos': ch_infos,
                'messages': store['x']
            }, f)


if __name__ == '__main__':
    token = os.environ['SLACK_API_TOKEN']
    sc = SlackClient(token)

    export_directory = 'slack-export-{}'.format(time.strftime('%Y-%m-%d-%H-%M-%S'))
    channels_directory = os.path.join(export_directory, 'channels')
    whisps_directory = os.path.join(export_directory, 'whisps')

    os.mkdir(export_directory)
    os.mkdir(channels_directory)
    os.mkdir(whisps_directory)

    print('SLACK EXPORT -', export_directory)
    print('CHANNELS     -', channels_directory)
    export_category(sc, 'public_channel, private_channel', channels_directory)
    print('WHISPS       -', whisps_directory)
    export_category(sc, 'mpim, im', whisps_directory)
    
    users_file = os.path.join(export_directory, 'users.json')
    print('USERS        -',  users_file)
    with open(users_file, 'w') as f:
        json.dump(sc.api_call('users.list', limit=50), f)
