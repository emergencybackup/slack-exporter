## Slack Conversations archiver

### Install 
```sh
pip install --user -r requirements.txt
```

### Usage
**Prerequisite**:  Generate a Slack token [Here](https://api.slack.com/custom-integrations/legacy-tokens)

```sh
SLACK_API_TOKEN=xoxp-***************************************************** python main.py
```


